# Variables
CWD := `docker compose exec php pwd`
EXEC := "docker compose exec php"
COMPOSER := EXEC + " composer"
SYMFONY := EXEC + " symfony"

# Aliases
composer +arguments:
  COMPOSER_ALLOW_SUPERUSER=1 {{COMPOSER}} {{arguments}}

console +arguments:
  {{SYMFONY}} console {{arguments}}

migration *arguments:
  {{SYMFONY}} console make:migration {{arguments}}

migrate:
  {{SYMFONY}} console doctrine:migrations:migrate --no-interaction

flush:
  {{SYMFONY}} console doctrine:database:drop --force
  {{SYMFONY}} console doctrine:database:create

reset: flush migrate

cc env='dev':
  {{SYMFONY}} console cache:clear --env={{env}}

install:
  {{COMPOSER}} install

new_app:
  {{SYMFONY}} new temp_dir
  {{EXEC}} rm -rf ./temp_dir/.git && cp -R ./temp_dir/. . && rm -rf ./temp_dir

test:
  {{EXEC}} php bin/phpunit
