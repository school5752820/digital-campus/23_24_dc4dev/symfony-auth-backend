<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class ApiController extends AbstractController
{
    #[Route(path: '/api/login', name: 'api_login', methods: ['POST'])]
    public function apiLogin(#[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
            return $this->json(
                'Retente ta chance',
                Response::HTTP_UNAUTHORIZED,
            );
        }

        return $this->json([
            'email' => $user->getEmail(),
        ], Response::HTTP_OK);
    }

    #[Route(path: '/api/me', name: 'api_me', methods: ['GET'])]
    public function apiMe(#[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
            return $this->json(null, Response::HTTP_UNAUTHORIZED);
        }

        return $this->json(null, Response::HTTP_OK);
    }
}