<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class PublicController extends AbstractController
{
    #[Route(path: '/', name: 'homepage')]
    public function index(): Response
    {
        return new Response('Accueil');
    }

    #[IsGranted('ROLE_USER')]
    #[Route('/protected', name: 'protected')]
    public function protectedRoute(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $email = $user->getEmail();
        return new Response("Protégé ! Ton email : $email");
    }
}